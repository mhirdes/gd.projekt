﻿using UnityEngine;
using System.Collections;

public class NonCharacter : MonoBehaviour {
	public float speed;
	float preferedSpeed;
	void Start () {
		animation.Stop ();
		animation.Play ("npcwalk");
		preferedSpeed = speed;
	}

	// Update is called once per frame
	void Update () {
		// Colliders Crosses
		int crossLayerMask = 1 << 12;
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.01f, crossLayerMask);
		if(hitColliders.Length > 0) {
			if(Random.value >= 0.7f) {
				turn();
			}
			moveForward();
		}

		// Colliders Wall
		int wallLayerMask = 1 << 11;
		Vector3 fwd = transform.forward * 10;
		Ray ray = new Ray(transform.position, fwd);
		if (Physics.Raycast (ray, 1.5f, wallLayerMask)) {
			turn ();
		} 
		moveForward ();
	}

	void turn() {
		speed = 0;
		float y = Mathf.Round(transform.rotation.y / 90) * 90;
		float direction = Random.Range (-1f, 1f);
		if (direction < 0) {
			y -= 90;
		} else {
			y += 90;
		}
		transform.Rotate (0,y,0);

	}

	void moveForward() {
		transform.Translate(Vector3.forward * speed * Time.deltaTime);
		speed = preferedSpeed;
	}
}
