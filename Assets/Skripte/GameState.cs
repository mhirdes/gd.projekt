﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour {

	float totalTime;
	public static float GameTime;
	public static int GameScore;
	public static int GameLives;
	public static int PowerUp;

	// Use this for initialization
	void Start () {
		PowerUp = 0;
		GameScore = 0;
		GameLives = 3;
		totalTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		GameTime = Time.time - totalTime;
	}
}
