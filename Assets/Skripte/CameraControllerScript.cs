﻿using UnityEngine;
using System.Collections;

public class CameraControllerScript : MonoBehaviour {

	public GameObject CameraPivot;
	public GameObject CameraObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		// Zuerst wird die ganze Gruppe durch die x-Bewegung der Maus gedreht
		transform.Rotate (new Vector3 (0, Input.GetAxis ("Mouse X"), 0));

		// Danach wird der Winkel der y-Bewegung der Maus zwischen 85 und 130 eingegrenzt
		float Angle = Mathf.Clamp(CameraPivot.transform.localEulerAngles.z - Input.GetAxis ("Mouse Y"), 85, 130);
		// Neigung der Kamera um den berechneten Wert
		CameraPivot.transform.localEulerAngles = new Vector3(0, 0, Angle);

		float Distance = Mathf.Clamp (CameraObject.transform.localPosition.y + Input.GetAxis("Mouse ScrollWheel"), -10, -2);
		CameraObject.transform.localPosition = new Vector3 (0, Distance, 0);
	}
}
