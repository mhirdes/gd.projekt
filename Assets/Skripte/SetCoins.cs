﻿using UnityEngine;
using System.Collections;

public class SetCoins : MonoBehaviour {

	public GameObject zero;
	public GameObject one;
	public float gridX = 5f;
	public float gridZ = 5f;
	public float gridY = 1f;
	public float spacing = 2f;
	
	void Start() {
		for (float z = 2.5f; z < gridZ; z+= spacing) {
			for (float x = 3.5f; x < gridX; x+=spacing) {
				Vector3 pos = new Vector3(x, gridY, z);
				Collider[] hitColliders = Physics.OverlapSphere(pos, 0.5f);
				if(hitColliders.Length == 0) {
					if(Random.Range(-1f,1f) < 0) {
						Instantiate(one, pos, Quaternion.identity);
					} else {
						Instantiate(zero, pos, Quaternion.identity);
					}
				}
			}
		}
	} 
}
