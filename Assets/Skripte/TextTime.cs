﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextTime : MonoBehaviour {
	Text txt;
	// Use this for initialization
	void Start () {
		txt = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		txt.text="Zeit: " + _CreateTimeString(GameState.GameTime);
	}
	// creates a formatted time string by given seconds
	
	private string _CreateTimeString(float Time){
		int minutes = (int)Time / 60;
		int seconds = (int)Time % 60;

		string minuteString = minutes.ToString();
		if(minutes < 10){
			minuteString = "0" + minuteString;    
		}

		string secondsString = seconds.ToString();
		if(seconds < 10){
			secondsString = "0" + secondsString;    
		}
		
		return minuteString + ":" + secondsString;    
		
	}
}
