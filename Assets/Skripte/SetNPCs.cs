﻿using UnityEngine;
using System.Collections;

public class SetNPCs : MonoBehaviour {
	
	public GameObject npc;
	public int number;
	private Vector3 npcStart = new Vector3(23.5f,1,27.5f);
	void Start() {
		for (int i = 0; i < number; i++) {
			Instantiate(npc, npcStart, Quaternion.identity);
		}
	} 
}
