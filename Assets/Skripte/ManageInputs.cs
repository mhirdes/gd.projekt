﻿using UnityEngine;
using System.Collections;

public class ManageInputs : MonoBehaviour {

	public GameObject CameraDefault;
	public GameObject CameraToSwitch;
	public GameObject PlayGUI;
	public GameObject PauseGUI;

	// Use this for initialization
	void Start () {
		CameraToSwitch.camera.depth = 1;
		CameraDefault.camera.depth = 2;
		PlayGUI.SetActive (true);
		PauseGUI.SetActive (false);
		Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.C)) {
			CameraToSwitch.camera.depth = 5;
			CameraDefault.camera.depth = 1;
			PlayGUI.SetActive(false);
		} 
		if(Input.GetKey (KeyCode.X)) {
			CameraToSwitch.camera.depth = 1;
			CameraDefault.camera.depth = 2;
			PlayGUI.SetActive(true);
		}

		//pause
		if(Input.GetKey (KeyCode.Space) || Input.GetKey(KeyCode.Escape)) {
			Time.timeScale = 0; 
			PlayGUI.SetActive(false);
			PauseGUI.SetActive(true);
		}
	}
}