﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	public float x;
	public float y;
	public float z;
	Vector3 rotation;

	// Use this for initialization
	void Start () {
		rotation = new Vector3(x,y,z); 
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.Rotate (rotation * Time.deltaTime * 200);
	}
}
