﻿using UnityEngine;
using System.Collections;

public class NPCCollision : MonoBehaviour {
	GameObject failedGUI;
	GameObject endGUI;
	public AudioClip failedAudio;
	public AudioClip bump;
	public AudioClip endAudio;
	private Vector3 npcStart = new Vector3(23.5f,0.04f,25.25f);

	// Use this for initialization
	void Start () {
		failedGUI = GameObject.Find ("FailedGUI");
		endGUI = GameObject.Find ("EndGUI");
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.tag == "NPC") {
			if(GameState.PowerUp > 0) {
				gameObject.GetComponent<AudioSource>().PlayOneShot(bump);
				GameState.GameScore += 100;
				other.gameObject.transform.position = npcStart;
				other.gameObject.transform.rotation = Quaternion.identity;
			} else {
				failed();
			}
		}
	}

	void failed() {
		if (Time.timeScale > 0) {
			Time.timeScale = 0;
			GameState.GameLives--;
		}
		if (GameState.GameLives > 0) {
			gameObject.GetComponent<AudioSource>().PlayOneShot(failedAudio);
			failedGUI.SetActive(true);
			foreach(GameObject NPC in GameObject.FindGameObjectsWithTag("NPC")) {
				NPC.transform.rotation = Quaternion.identity;
				NPC.transform.position = npcStart;
			}
			gameObject.transform.position = new Vector3 (25, 1, 12.5f);
			gameObject.transform.rotation = Quaternion.LookRotation(new Vector3(0,0,1));
		} else {
			gameObject.GetComponent<AudioSource>().PlayOneShot(endAudio);
			endGUI.SetActive(true);
		}
	}
}
