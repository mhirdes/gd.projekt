﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ManageGUI : MonoBehaviour {

	public AudioClip buttonAudio;

	void Start () {
		SetButtonSounds ();
		GameObject[] guisToHide = GameObject.FindGameObjectsWithTag ("GUI");
		foreach (GameObject guiToHide in guisToHide) {
			guiToHide.SetActive(false);
		}
	}

	public void ChangeToScene (string sceneToChangeTo) {
		Application.LoadLevel(sceneToChangeTo);
	}

	public void DisableGUI (GameObject GUIToDisable) {
		GUIToDisable.SetActive (false);
	}

	public void EnableGUI (GameObject GUIToEnable) {
		GUIToEnable.SetActive (true);
	}

	public void ResumeGame () {
		Time.timeScale = 1;
	}

	public void Exit() {
		Application.Quit();
	}

	public void SetButtonSounds() {
		GameObject[] allButtons = GameObject.FindGameObjectsWithTag ("Button");
		foreach (GameObject button in allButtons) {
			button.GetComponent<Button>().onClick.AddListener(() => PlayButtonSound());
		}

	}

	public void PlayButtonSound() {
		gameObject.GetComponent<AudioSource> ().PlayOneShot (buttonAudio);
	}
}
