﻿using UnityEngine;
using System.Collections;

public class CollectItems : MonoBehaviour {

	public GameObject EndGUI;
	AudioSource[] coinAudio;
	Light light;
	AudioSource[] managerAudio;
	// Use this for initialization
	void Start () {
		coinAudio = gameObject.GetComponents<AudioSource> ();
		light = GameObject.Find("Light").GetComponent<Light> ();
		managerAudio = GameObject.Find ("_Manage").GetComponents<AudioSource> ();

	}
	
	// Update is called once per frame
	void Update () {
		GameObject[] coins = GameObject.FindGameObjectsWithTag("Coin");
		// won the game
		if(coins.Length == 0 && GameState.GameLives > 0) {
			Time.timeScale = 0;
			GameState.GameLives = 0;
			GameState.GameScore += 1000;
			coinAudio [3].Play ();
			EndGUI.SetActive(true);
		}
	}

	void OnTriggerEnter(Collider other) {
		// Coin
		if (other.gameObject.tag == "Coin") {
			coinAudio[0].Play();
			GameState.GameScore += 10;
			Destroy(other.gameObject);
		}
		// PowerUp
		if (other.gameObject.name == "PowerUp") {
			GameState.PowerUp += 1;
			GameState.GameScore += 50;
			light.intensity = 0.3f;
			coinAudio[1].Play();
			switchBackgroundMusic(0,1);
			Destroy(other.gameObject);
			StartCoroutine(afterTime());
		}
	}
	IEnumerator afterTime() {
		yield return new WaitForSeconds (8);
		if (GameState.PowerUp == 1) {
			coinAudio [2].Play ();
		}
		yield return new WaitForSeconds (2);
		if (GameState.PowerUp == 1) {
			light.intensity = 0.7f;
			switchBackgroundMusic(1,0);
		}
		GameState.PowerUp -= 1;
	}

	void switchBackgroundMusic(int stop, int start) {
		managerAudio [stop].Stop ();
		managerAudio [start].Play ();
	}
}
