﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextLives : MonoBehaviour {
	Text txt;
	// Use this for initialization
	void Start () {
		txt = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		txt.text="Leben: " + GameState.GameLives;
	}
}
