﻿using UnityEngine;
using System.Collections;
public class CameraCollision : MonoBehaviour {
	public float minDistance = 1.0f; 
	public float maxDistance = 2.0f;

	Vector3 dollyDir; 
	float distance;
	GameObject player;

	void Awake() { 
		dollyDir = transform.localPosition.normalized; 
		distance = transform.localPosition.magnitude; 
		player = GameObject.Find ("Player");
	}

	void Update() { 
		
		Vector3 desiredCameraPos = transform.parent.TransformPoint( dollyDir * distance );
		
		// (optional) put layers you don't want to collide with  here (probably things like enemies)
		int layerMask = 1 << 11; 
		
		RaycastHit hit;
		if( Physics.Linecast( player.transform.position, desiredCameraPos, out hit, layerMask ) )
		{
			distance = Mathf.Clamp( hit.distance, minDistance, maxDistance );
		}
		
		transform.localPosition = dollyDir * distance;
	}
}